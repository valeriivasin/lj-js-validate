var exec = require('child_process').exec;
var fs = require('fs');

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish-ex');
var gulpif = require('gulp-if');

var jshintJunitReporter = require('jshint-junit-reporter');

var gcallback = require('gulp-callback');

var hooker = require('hooker');

var fixmyjs = require('gulp-fixmyjs');

var FOLDER = 'files';
var src = [
  FOLDER + '/**/*.js',
  '!' + FOLDER + '/**/*.min.js',
  '!' + FOLDER + '/ace/**',
  '!' + FOLDER + '/apps/**',
  '!' + FOLDER + '/ads/**',
  '!' + FOLDER + '/ck/**',
  '!' + FOLDER + '/ckeditor/**',
  '!' + FOLDER + '/dev/**',
  '!' + FOLDER + '/ImageRegionSelect/**',
  '!' + FOLDER + '/lib/**',
  '!' + FOLDER + '/deprecated/**'
];

var today = new Date();
var MONTH_AGO = today.setMonth(today.getMonth() - 1);

gulp.task('download', function (callback) {
  exec('rsync -az lj:/home/lj/htdocs/js/ ./files/', callback);
});

gulp.task('jshint', ['download'], function () {

  return gulp.src(src)

    .pipe(gulpif(function (file) {
      var mtime = fs.statSync(file.path).mtime;
      return mtime > MONTH_AGO;
    }, jshint()))

    .pipe(jshint.reporter(stylish));
});


gulp.task('jshint:jenkins', ['download'], function () {
  var output = '';
  var jshintReport = 'jshint.xml';
  var strToReplace = '<?xml version="1.0" encoding="utf-8"?>\n';

  hooker.hook(process.stdout, 'write', {
    pre: function(out) {
      var start = out.indexOf('<testcase');
      var end = out.indexOf('</testcase>') + 11;
      output += out.slice(start, end);
      return hooker.preempt();
    }
  });

  return gulp.src(src)

    .pipe(gulpif(function (file) {
      var mtime = fs.statSync(file.path).mtime;
      return mtime > MONTH_AGO;
    }, jshint()))

    .pipe(jshint.reporter(jshintJunitReporter))

    .pipe(gcallback(function () {
      fs.writeFile(jshintReport, [
        strToReplace,
        '<testsuite name="jshint">',
        output,
        '</testsuite>'
      ].join(''));
    }));
});

gulp.task('fix:prepare', ['download'], function () {
  return gulp.src(src)
    .pipe(gulpif(function (file) {
      var mtime = fs.statSync(file.path).mtime;
      return mtime > MONTH_AGO;
    }, fixmyjs()))
    .pipe(gulp.dest(FOLDER));
});

gulp.task('fix', ['fix:prepare'], function (callback) {
  exec('rsync -ax ./files/ lj:/home/lj/htdocs/js/', callback);
});
