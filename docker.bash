#!/usr/bin/env bash
DIR=$(pwd)
docker run -p 8080:8080 -v $DIR/jenkins_home:/var/jenkins_home -v $DIR/.ssh:/var/jenkins_home/.ssh livejournal/jenkins
