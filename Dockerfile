FROM jenkins

USER root
RUN apt-get update
RUN apt-get install -y nodejs nodejs-legacy npm git git-core
RUN npm install -g gulp
USER jenkins
